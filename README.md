git clone git@bitbucket.org:esazykin/trans-score-user.git

You may to configure a local db connection. For this create a file app/config/db.local.php
Otherwise, use a default db connection app/config/db.php

Run a command below, for create or update db schema

vendor/bin/doctrine orm:schema-tool:update --force --dump-sql