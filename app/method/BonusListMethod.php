<?php

namespace app\method;

use app\domain\exception\PopulateException;
use app\domain\form\AccrualForm;
use app\domain\model\Accrual;
use app\rpc\response\body\RpcResponseResult;

class BonusListMethod extends Method
{
	public function run(array $params)
	{
		$form = new AccrualForm();
		try {
			$form->setAttributes($params);
		} catch (PopulateException $e) {
			return $this->createError([$e->getField() => $e->getMessage()]);
		}
		$repository = $this->doctrine->getRepository(Accrual::class);
		$r = $repository->findBy($form->getCriteria());

		return new RpcResponseResult(array_map(function(Accrual $v) { return $v->getAttributes(); }, $r));
	}
}