<?php

namespace app\method;

use app\domain\exception\PopulateException;
use app\domain\model\Accrual;
use app\domain\model\User;
use app\domain\validator\AccrualValidator;
use app\domain\validator\UserValidator;
use app\rpc\response\body\RpcResponseBody;
use app\rpc\response\body\RpcResponseResult;
use DateTime;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

/*
var data = {id: 1, jsonrpc: 2};
data.method = "addBonus";
data.params = {
  users: [{
    id: 1,
    phone: 34343434343,
    partnerId: 343
  }, {
    id: 10,
    phone: 1234567,
    partnerId: 343
  }],
  accruals: [
    {
      amount:200,
      orderId: 10041,
      score: 4040,
      userId: 1
    }, {
      amount:200.0,
      orderId: 10051,
      score: 4040,
      userId: 10
    }
  ]
}

$.ajax({
  headers: {
    'Content-Type': 'application/json'
  },
  type: "post",
  data: JSON.stringify(data),
  url: '/api'
})
 */


class BonusAddMethod extends Method
{
	/**
	 * @param array $params
	 * @return RpcResponseBody
	 */
	public function run(array $params)
	{
		if (!empty($params['users']) && !is_array($params['users'])) {
			return $this->createError(['users' => lang('error')['type_mismatch']]);
		}
		if (empty($params['accruals'])) {
			return $this->createError(['accruals' => lang('error')['missing']]);
		}
		if (!is_array($params['accruals'])) {
			return $this->createError(['accruals' => lang('error')['type_mismatch']]);
		}

		try {
			$validator = new AccrualValidator();
			foreach ($params['accruals'] as $accrualParams) {
				$accrual = new Accrual();
				$accrual->setCreated(new DateTime());
				$accrual->populate($accrualParams);
				if (!$validator->validate($accrual)) {
					return $this->createError($validator->getErrors());
				}
				$this->doctrine->persist($accrual);
			}

			if (!empty($params['users'])) {
				$validator = new UserValidator();
				$userRepository = $this->doctrine->getRepository(User::class);
				foreach ($params['users'] as $userParams) {
					$user = new User();
					$user->populate($userParams);
					if ($userRepository->find($user->getId())) {
						continue;
					}
					if (!$validator->validate($user)) {
						return $this->createError($validator->getErrors());
					}
					$this->doctrine->persist($user);
				}
			}
		} catch (PopulateException $e) {
			return $this->createError([$e->getField() => $e->getMessage()]);
		}

		try {
			$this->doctrine->flush();
		} catch (UniqueConstraintViolationException $e) {
			return $this->createError(['orderId' => lang('error')['not_unique']]);
		}

		return new RpcResponseResult(true);
	}
}
