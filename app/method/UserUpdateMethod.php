<?php

namespace app\method;

use app\domain\model\User;
use app\rpc\response\body\RpcResponseResult;

class UserUpdateMethod extends Method
{
	public function run(array $params)
	{
		$userRepository = $this->doctrine->getRepository(User::class);
		foreach ($params as $userParams) {
			/** @var User $user */
			$user = $userRepository->find($userParams['id']);
			if (!$user) {
				$msg = lang('error')['object_not_found'];
				$msg = sprintf($msg, 'User#' . $userParams['id']);
				return $this->createError(['id' => $msg]);
			}
			$user->setPhone($userParams['phone']);

			$this->doctrine->persist($user);
		}

		$this->doctrine->flush();

		return new RpcResponseResult(true);
	}
}