<?php

namespace app\mybillMessage;

use app\data\MybillClient\MybillClientInterface;

class Presenter
{
	private $client;

	public function __construct(MybillClientInterface $client)
	{
		$this->client = $client;
	}

	public function request($params)
	{
		$resp = $this->client->doRequest($params);
	}
}