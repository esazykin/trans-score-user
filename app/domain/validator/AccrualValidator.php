<?php

namespace app\domain\validator;

use app\domain\model\Accrual;
use app\domain\model\Model;

class AccrualValidator extends Validator
{
	/**
	 * @param Accrual $model
	 */
	public function validate(Model $model)
	{
		if (!is_int($model->getOrderId())) {
			$this->errors['orderId'] = lang('error')['type_mismatch'];
		}
		if (!is_int($model->getAmount())) {
			$this->errors['amount'] = lang('error')['type_mismatch'];
		}
		if (!is_int($model->getScore())) {
			$this->errors['score'] = lang('error')['type_mismatch'];
		}
		if (!is_int($model->getUserId())) {
			$this->errors['userId'] = lang('error')['type_mismatch'];
		}

		return !$this->hasErrors();
	}
}