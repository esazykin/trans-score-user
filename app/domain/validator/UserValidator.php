<?php

namespace app\domain\validator;

use app\domain\model\Model;
use app\domain\model\User;

class UserValidator extends Validator
{
	/**
	 * @param User $model
	 */
	public function validate(Model $model)
	{
		if (!empty($model->getPhone()) && !is_int($model->getPhone())) {
			$this->errors['phone'] = lang('error')['type_mismatch'];
		}
		if (!is_int($model->getPartnerId())) {
			$this->errors['partnerId'] = lang('error')['type_mismatch'];
		}
		if (!is_int($model->getId())) {
			$this->errors['id'] = lang('error')['type_mismatch'];
		}

		return !$this->hasErrors();
	}
}