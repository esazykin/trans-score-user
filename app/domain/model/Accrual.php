<?php

namespace app\domain\model;
use DateTime;

/**
 * @Entity(repositoryClass="app\data\AccrualRepository")
 * @Table(name="accrual", uniqueConstraints={@UniqueConstraint(name="idx1", columns={"orderId"})})
 */
class Accrual extends Model
{
	const STATUS_TAKEN = 0;
	const STATUS_PROCESSED = 1;

	/**
	 * @Column(type="integer")
	 * @var int
	 */
	protected $orderId;

	/**
	 * @Column(type="integer")
	 * @var int
	 */
	protected $userId;

	/**
	 * @Column(type="float")
	 * @var float
	 */
	protected $amount;

	/**
	 * @Column(type="integer")
	 * @var int
	 */
	protected $score;

	/**
	 * @Column(type="integer")
	 * @var int
	 */
	protected $status = self::STATUS_TAKEN;

	/**
	 * @Column(type="datetime")
	 * @var DateTime
	 */
	protected $created;

	public function getOrderId()
	{
		return $this->orderId;
	}

	public function setOrderId($orderId)
	{
		$this->orderId = $orderId;
	}

	public function getScore()
	{
		return $this->score;
	}

	public function setScore($score)
	{
		$this->score = $score;
	}

	public function getCreated()
	{
		return $this->created;
	}

	public function setCreated($created)
	{
		$this->created = $created;
	}

	public function getAmount()
	{
		return $this->amount;
	}

	public function setAmount($amount)
	{
		$this->amount = $amount;
	}

	public function getUserId()
	{
		return $this->userId;
	}

	public function setUserId($userId)
	{
		$this->userId = $userId;
	}

	public function getStatus()
	{
		return $this->status;
	}

	public function setStatus($status)
	{
		$this->status = $status;
	}
}