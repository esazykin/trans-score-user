<?php

namespace app\domain\model;

/**
 * @Entity
 * @Table(name="user")
 */
class User extends Model
{
	/**
	 * @Id @Column(type="integer", options={"unsigned"=true})
	 * @var int
	 */
	protected $id;

	/**
	 * @Column(type="bigint", unique=true, options={"unsigned"=true})
	 * @var int
	 */
	protected $phone;

	/**
	 * @Column(type="integer")
	 * @var int
	 */
	protected $partnerId;

	public function getPhone()
	{
		return $this->phone;
	}

	public function setPhone($phone)
	{
		$this->phone = $phone;
	}

	public function getPartnerId()
	{
		return $this->partnerId;
	}

	public function setPartnerId($partnerId)
	{
		$this->partnerId = $partnerId;
	}

	public function setId($id)
	{
		$this->id = $id;
	}
}