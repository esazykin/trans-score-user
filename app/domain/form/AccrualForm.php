<?php

namespace app\domain\form;

use app\domain\model\Accrual;

class AccrualForm extends Form
{
	protected $modelClass = Accrual::class;

	protected $id;

	public function getCriteria()
	{
		$criteria = $this->model->getAttributes();
		$criteria['id'] = $this->id;
		return array_filter($criteria);
	}

	public function setId($id)
	{
		$this->id = $id;
	}
}