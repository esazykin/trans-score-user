<?php

namespace app\rpc\validator;


use app\rpc\request\RpcRequestBase;
use app\rpc\response\body\RpcResponseError;

class RpcRequestValidator extends RpcRequestValidatorBase
{
	/**
	 * @var string
	 */
	private $methodNamespace;

	public function __construct($methodNamespace)
	{
		$this->methodNamespace = $methodNamespace;
	}

	public function validate(RpcRequestBase $rpcMessage)
	{
		if (empty($rpcMessage->getId())) {
			$this->error = new RpcResponseError();
			$this->error->setCode(RpcResponseError::CODE_INVALID_REQUEST);
			return false;
		}

		if (empty($rpcMessage->getVersion()) || $rpcMessage->getVersion() != '2.0') {
			$this->error = new RpcResponseError();
			$this->error->setCode(RpcResponseError::CODE_INVALID_REQUEST);
			return false;
		}

		if (empty($rpcMessage->getMethod())) {
			$this->error = new RpcResponseError();
			$this->error->setCode(RpcResponseError::CODE_INVALID_REQUEST);
			return false;
		}
		$cls = $this->methodNamespace . '\\' . ucfirst($rpcMessage->getMethod()) . 'Method';
		if (!class_exists($cls)) {
			$this->error = new RpcResponseError();
			$this->error->setCode(RpcResponseError::CODE_METHOD_NOT_FOUND);
			return false;
		}

		if (!is_array($rpcMessage->getParams())) {
			$this->error = new RpcResponseError();
			$this->error->setCode(RpcResponseError::CODE_INVALID_REQUEST);
			return false;
		}

		return true;
	}
}