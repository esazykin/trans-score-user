<?php

namespace app\rpc\validator;


use app\rpc\request\RpcRequestBase;
use app\rpc\response\body\RpcResponseError;

abstract class RpcRequestValidatorBase
{
	/**
	 * @var RpcResponseError
	 */
	protected $error;
	
	abstract public function validate(RpcRequestBase $rpcMessage);

	/**
	 * @return RpcResponseError
	 */
	public function getError()
	{
		return $this->error;
	}
}