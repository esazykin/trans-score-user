<?php

namespace app\rpc\response;

use app\rpc\response\body\RpcResponseBody;
use JsonSerializable;

class RpcResponse implements JsonSerializable
{
	/**
	 * @var string
	 */
	private $id;

	/**
	 * @var string - version of protocol
	 */
	private $jsonrpc;

	/**
	 * @var RpcResponseBody
	 */
	private $body;

	public function __construct($id, $version, RpcResponseBody $body)
	{
		$this->id = $id;
		$this->jsonrpc = $version;
		$this->body = $body;
	}

	public function jsonSerialize()
	{
		return ['id' => $this->id, 'jsonrpc' => $this->jsonrpc] + $this->body->getBody();
	}
}