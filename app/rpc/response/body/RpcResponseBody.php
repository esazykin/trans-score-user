<?php

namespace app\rpc\response\body;

interface RpcResponseBody
{
	/**
	 * @return array
	 */
	public function getBody();
}