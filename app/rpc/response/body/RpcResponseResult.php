<?php

namespace app\rpc\response\body;


class RpcResponseResult implements RpcResponseBody
{
	/**
	 * @var mixed
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * @return array
	 */
	public function getBody()
	{
		return ['result' => $this->data];
	}
}