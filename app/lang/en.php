<?php

return [
	'error' => [
		'missing' => 'Field not found',
		'object_not_found' => '%s not found',
		'type_mismatch' => 'Type mismatch',
		'not_unique' => 'The field already used',
	],
	'rpcResponseError' => [
		-32700 => 'Parse error',
		-32600 => 'Invalid Request',
		-32601 => 'Method not found',
		-32602 => 'Invalid params',
		-32603 => 'Internal error',
		-32000 => 'Server error',
	],
];