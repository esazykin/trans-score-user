<?php

namespace app\data\MybillClient;


interface MybillClientInterface
{
	public function doRequest(array $params);
}