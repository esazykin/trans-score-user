<?php

namespace app\data\mybillClient;

use GuzzleHttp\Client;

class MybillClient implements MybillClientInterface
{
	protected $client;

	protected $method;

	protected $url;

	public function __construct($method, $url)
	{
		$this->client = new Client();
		$this->method = $method;
		$this->url = $url;
	}

	public function doRequest(array $params)
	{
		$res = $this->client->request($this->method, $this->url, [
			'body' => ['user', 'pass']
		]);
		$status = $res->getStatusCode(); // 200
		$response = $res->getBody();
		return $response;
	}
}
