<?php

namespace console\command;

use app\domain\model\Accrual;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BonusAddCommand extends Command
{
	/**
	 * @var EntityManager
	 */
	protected $doctrine;

	public function setEntityManager(EntityManager $doctrine)
	{
		$this->doctrine = $doctrine;
		return $this;
	}


	protected function configure()
	{
		$this
			->setName('bonus-add')
			->setDescription('Send bonuses to "Loyalica"');
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$accrualRepository = $this->doctrine->getRepository(Accrual::class);
		$accruals =$accrualRepository->findBy(['status' => Accrual::STATUS_TAKEN]);

		if (count($accruals)) {

			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL,"https://www.test.mybill.ru/api_cmd");
			curl_setopt($ch, CURLOPT_POST, 1);
//			curl_setopt($ch, CURLOPT_POSTFIELDS,
//				"postvar1=value1&postvar2=value2&postvar3=value3");

			$post = [
				'login' => 'goncharov@iway.ru',
            	'passwd' => md5('mike2404'),
            	'cmd' => rawurlencode(json_encode([
					'add' => [
						'amount' => 10,
                        'guid' => 'c59317ec-05c3-d25f-6067-d022d521d370',
                        'email' => null,
                        // phone: v.phone,
                        'phone' => "+79261234567",
                        'uniqId' => 1
                    ],
					'typeOfCommand' => "addBonus"
                ]))
			];
			$post = http_build_query([
				'command' => json_encode($post)]
			);


			curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$server_output = curl_exec ($ch);

			curl_close ($ch);

		}

		$output->writeln("XXX");
	}
}